from boto3 import client as boto3Client   # Connect to AWS services e.g. s3
from os import environ   # Dictionary containing the environment variables
from datetime import datetime   # For "created-date" tag
from json import dumps as jsonDumpStr   # Make a json string from a dictionary
from time import perf_counter   # Timing function

from base64 import b64decode   # Decode base64 POST request
from email.parser import Parser as emailParser   # For parsing POST request data
from json import loads as jsonLoadStr    # Convert string to json dictionary
from io import BytesIO   # Convert binary data to a file-like byte stream
from zipfile import ZipFile   # Can read zipfile byte streams
from threading import Thread   # Multithreading for small file uploads



S3 = boto3Client('s3')
kwStatic = {   # Common keywords for uploading to S3 bucket -- the rest depend on the file
    'Bucket' : environ['S3Bucket'],
    'Tagging' : 'company-name=derivco-sa'
                '&environment=snd'
                '&project-name=easy-help'
                '&owned-by=cnd@tech-connect.com.au'
                '&created-by=jeremy@tech-connect.com.au'
                '&created-date='+datetime.now().strftime('%d/%m/%Y')+
                '&business-unit=slipstream'
}
timeStr = '{:.6f}'   # Common timer format for return response


def respond(metadata, err, statusCode, message):
    '''
    Construct a json response call. The body will contain the URLs of zipped and
    unzipped data, IDs from the json document, file counts and sizes, and timing
    metrics. The status code should be 200 for success and 400 for failure, with
    the type of error being diverted through as well.
    '''
    return {
        'statusCode' : statusCode, 
        'headers' : {'Content-Type' : 'application/json'},
        'body' : jsonDumpStr({
            **metadata,
            'ErrorType' : type(err).__name__,
            'Message' : message
            })
        }


def lambda_handler(event, context):
    '''
    The user must upload a json file containing ID metadata, along with a zip
    file containing the help files. These files are checked and then we make two
    uploads to S3: the zip still compressed and its contents uncompressed.
    
    This function implements the threading module to upload concurrently. The
    amount of speed-up is proportional to the memory allocated to the function.
    '''
    metadata = {}   # Dictionary to return URLs, IDs, timing, file sizes, etc.
    
    
    # Read the headers to get the boundary string that separates parts (uploads)
    # ... then decode the body (upload data and metadata) from base 64 encoding
    t = perf_counter()   # ReadingTime
    try:
        contentType = event['headers']['content-type']
        assert(contentType.split(';')[0] == 'multipart/form-data')   # Type of body must be uploads
        boundary = contentType.split('boundary=')[-1].encode()   # Convert to bytes; utf-8 encoding is fine
        bodyBytes = b64decode(event['body'])
                    
    except Exception as err:
        return respond(metadata, err, 400, 'Invalid file uploads')
    metadata['ReadingTime'] = timeStr.format(perf_counter()-t)
        
        
    jsonFile = None   # json file name, or None if not found yet
    zipFile = None   # same with zip
    zipList = []   # Zip data may be split over several lines due to Python and
                   # \r\n, so we track all parts to later stitch them together
    
    
    # Parse through the message body manually.
    t = perf_counter()   # ParsingTime
    for part in bodyBytes.split(boundary):   # Each part is divided by the boundary byte string
        contentType = None   # This will be the type of file upload
        
        # TODO rearrange this into two loops: the first for just metadata and the second for just data
        for line in part.split(b'\r\n'):   # Read the part line by line
            #print(line[:50])
            if not line or line == b'--': continue   # Ignore filler lines
            
            # Content type is found in the metadata; once it's known we can read the actual data
            if contentType == 'application/json':
                try:
                    inputJSON = jsonLoadStr(line.decode())   # Convert bytes to string to json-like dictionary
                    metadata['ModuleId'] = str(inputJSON['ModuleId'])
                    metadata['ClientIds'] = str(inputJSON['ClientIds'])
                    metadata['BuildId'] = str(inputJSON['BuildId'])
                    continue
                             
                except Exception as err:
                    return respond(metadata, err, 400, f'{jsonFile} is an invalid json file')
               
            # Zip file. There are several zip content types so this line may need to be adjusted
            elif contentType == 'application/x-zip-compressed':
                zipList.append(line)
                continue
                   
            # Any other content type returns an error
            elif contentType:
                return respond(metadata, IOError(), 400, f'{filename} is of invalid content type {contentType}')
                
                
            # Read the metadata which is given at the top of the body
            for segment in line.split(b';'):   # Metadata fields are often separated by semicolons
            
                if b'filename' in segment:
                    filename = segment.split(b'"')[1].decode()   # Bytes to string
                    extension = filename.split('.')[-1]
                    if extension == 'json':
                        if jsonFile: return respond(metadata, IOError(), 400, 'Multiple json files')
                        jsonFile = filename
                    if extension == 'zip': 
                        if zipFile: return respond(metadata, IOError(), 400, 'Multiple zip files')
                        zipFile = filename
                
                if b'Content-Type' in segment:
                    contentType = segment.split(b'Content-Type: ')[-1].decode()
        
                    
    if not jsonFile: return respond(metadata, IOError(), 400, 'No json file')
    if not zipFile: return respond(metadata, IOError(), 400, 'No zip file')
    metadata['ParsingTime'] = timeStr.format(perf_counter()-t)
    
    
    # Try the zip file by getting its contents; if it's bad, it will fail here
    # ... then upload the uncompressed zip to the S3 bucket
    t = perf_counter()
    zipRaw = b'\r\n'.join(zipList)
    try:
        zipIO = BytesIO(zipRaw)   # Convert bytes into file-like byte stream
        with ZipFile(zipIO) as zipObject:   # Open the zip file
            fileList = zipObject.namelist()   # Files and directory structure
            S3.put_object(**kwStatic, Key=zipFile, Body=zipRaw, ContentType='application/zip')   # Upload to S3
    
    except Exception as err:
        return respond(metadata, err, 400, 'Invalid zip file')
    metadata['ZipURL'] = environ['S3BucketURL'] + zipFile
    metadata['ZipBytes'] = str(len(zipRaw))
    metadata['UnzippingTime'] = timeStr.format(perf_counter()-t)


    # Upload files to the S3 bucket easyhelp-html using multi-threading concurrency
    t = perf_counter()
    dataBytes = 0   # Unzipped size of the contents
    fileCount = 0   # Number of actual files (not directories) in the archive
    try:
        threads = []   # The task for each thread will be stored in this list
        with ZipFile(zipIO) as zipObject:
            for file in fileList:
                with zipObject.open(file) as data:
                    key = file
                    if key.split('/')[0] == 'public':
                        key = key.replace('public/', '', 1)   # Remove public directory
                    key = metadata['BuildId'] + '/' + key   # Append build ID on front
                    body = data.read()
                    
                    # Ensure the upload has the appropriate content type so it will open in S3
                    contentType = 'application/octet-stream'   # Default content type
                    if not zipObject.getinfo(file).is_dir():   # If it is a file
                        fileCount += 1
                        extension = file.split('.')[-1]
                        if extension == 'html':
                            contentType = 'text/html'
                        elif extension == 'jpg':
                            contentType = 'image/jpeg'
                        elif extension == 'js':
                            contentType = 'text/javascript'
                        elif extension == 'json':
                            contentType = 'application/json'
                        elif extension == 'txt':
                            contentType = 'text/plain'
                    
                    threads.append(Thread(   # Create the task for a thread
                        target = S3.put_object,
                        kwargs = {**kwStatic, 'Key':key, 'Body':body, 'ContentType':contentType}
                    ))
                    
                    dataBytes += len(body)

        # Start all threads running
        for thread in threads:
            thread.start()
            
        # Wait for all threads to finish
        for thread in threads:
            thread.join()

    except Exception as err:
        return respond(metadata, err, 400, f"Could not upload to S3 bucket {environ['S3Bucket']}")
        
    metadata['DataURL'] = environ['S3BucketURL'] + metadata['BuildId'] + '/index.html'
    metadata['DataBytes'] = str(dataBytes)
    metadata['FileCount'] = str(fileCount)
    metadata['FolderCount'] = str(len(fileList)-fileCount)
    metadata['UploadingTime'] = timeStr.format(perf_counter()-t)

        
    return respond(metadata, None, 200, 'Success!')
    