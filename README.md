submitJob Lambda function Python code for the EasyHelp project

Take a POST request containing a json file and a zip file. The json should store metadata relating to the build and the zip will contain the built help html files. The zip file is uploaded compressed, and its contents unzipped and uploaded as well, with a return message pointing the user to their files in the bucket.